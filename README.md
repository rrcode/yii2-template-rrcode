Yii 2 Basic For RR-Code
============================

Yii 2 Basic Template is a skeleton [Yii 2] application best for
rapidly creating small projects.

The template contains the basic features, it includes all commonly used configurations that would allow you to focus on adding new
features to your application.


DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

### Install from an Archive File

1.- update composer
~~~
php composer.phar global require "fxp/composer-asset-plugin:~1.2.0"
~~~

2.- install libraries
~~~
php composer.phar install
~~~

3.- create database: rrcode

4.- migrate tables in database

~~~
php yii migrate
~~~



You can then access the application through the following URL:

~~~
http://localhost/basic/web/
~~~




CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

