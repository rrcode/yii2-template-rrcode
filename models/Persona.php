<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "persona".
 *
 * @property integer $id
 * @property string $nombres
 * @property string $apellido_paterno
 * @property string $apellido_materno
 * @property string $ci
 * @property string $correo
 * @property integer $telefono
 * @property integer $movil
 * @property string $genero
 * @property string $estado_civil
 * @property string $fecha_nacimiento
 */
class Persona extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'persona';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['telefono', 'movil'], 'integer'],
            [['movil'], 'required'],
            [['fecha_nacimiento'], 'safe'],
            [['nombres'], 'string', 'max' => 110],
            [['apellido_paterno', 'apellido_materno'], 'string', 'max' => 50],
            [['ci'], 'string', 'max' => 60],
            [['correo'], 'string', 'max' => 100],
            [['genero'], 'string', 'max' => 10],
            [['estado_civil'], 'string', 'max' => 11],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nombres' => 'Nombres',
            'apellido_paterno' => 'Apellido Paterno',
            'apellido_materno' => 'Apellido Materno',
            'ci' => 'Ci',
            'correo' => 'Correo',
            'telefono' => 'Telefono',
            'movil' => 'Movil',
            'genero' => 'Genero',
            'estado_civil' => 'Estado Civil',
            'fecha_nacimiento' => 'Fecha Nacimiento',
        ];
    }
}
