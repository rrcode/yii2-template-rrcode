<?php

use yii\db\Migration;

class m160715_132027_create_table_persona extends Migration
{
    public function up()
    {
      $this->createTable('persona', [
          'id'               => $this->primaryKey(),
          'nombres'          => $this->string(110),
          'apellido_paterno' => $this->string(50),
          'apellido_materno' => $this->string(50),
          'ci'               => $this->string(60),
          'correo'           => $this->string(100),
          'telefono'         => $this->integer(),
          'movil'            => $this->integer()->notNull(),
          'genero'           => $this->string(10),
          'estado_civil'     => $this->string(11),
          'fecha_nacimiento' => $this->date()
      ]);
    }

    public function down()
    {
        $this->dropTable('persona');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
