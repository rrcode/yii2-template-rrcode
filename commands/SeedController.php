<?php

namespace app\commands;
use yii\console\Controller;
use app\models\Persona;
class SeedController extends Controller
{
  public function actionIndex()
    {
        $faker = \Faker\Factory::create();

        $persona = new Persona();
        for ( $i = 1; $i <= 20; $i++ )
        {
            $persona->setIsNewRecord(true);
            $persona->id = null;

            $persona->nombres          = $faker->name;
            $persona->apellido_paterno = $faker->firstName;
            $persona->apellido_materno = $faker->lastName;
            $persona->ci               = $faker->postcode;
            $persona->correo           = $faker->email;
            $persona->telefono         = $faker->randomNumber;
            $persona->movil            = $faker->randomNumber;
            $persona->genero           = $faker->randomElement($array = array ('M','F'));
            $persona->estado_civil     = $faker->randomElement($array = array ('C','S','V'));
            $persona->fecha_nacimiento = $faker->date($format = 'Y-m-d', $max = 'now');
            $persona->save();

        }
    }
}
