<?php

$params = require(__DIR__ . '/params.php');
use kartik\datecontrol\Module;
$config = [
    'id' => 'yii2-template-rrcode',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'J3WM5gPGxHevbr7iKfwtQuLkktVkMyhV',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'loginUrl' => ['site/login'],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],

        'db' => require(__DIR__ . '/db.php'),
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'modules' => [
        'gridview' => [
            'class' => '\kartik\grid\Module'
        ],
        'datecontrol' => [
            'class' => '\kartik\datecontrol\Module',
            'displaySettings' => [
                Module::FORMAT_DATE => 'php:D j M Y',
                Module::FORMAT_TIME => 'H:i:s',
                Module::FORMAT_DATETIME => 'php:D j M Y H:i:s'
            ],
            'autoWidgetSettings' => [
                Module::FORMAT_DATE => [
                    'type' => 2,
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ], // example
                Module::FORMAT_DATETIME => [
                    'type' => 2,
                    'pluginOptions' => [
                        'autoclose' => true
                    ]
                ], // setup if needed
                Module::FORMAT_TIME => [ ]
            ], // setup if needed
            'saveSettings' => [
                Module::FORMAT_DATE => 'yyyy/M/dd', // saves as unix timestamp
                Module::FORMAT_TIME => 'H:i:s',
                Module::FORMAT_DATETIME => 'yyyy/M/dd H:i:s'
            ],
            'autoWidget' => true,
            // 'autoClose' => true,
            'widgetSettings' => [
                Module::FORMAT_DATE => [
                    'class' => 'yii\jui\DatePicker',
                    'options' => [
                        'options' => [
                            'class' => 'form-control'
                        ],
                        'clientOptions' => [
                            'dateFormat' => 'dd-mm-yy'
                        ],
                        'style' => 'z-index:9999 !important',
                    ]
                ]
            ]
        ]
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
