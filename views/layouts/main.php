<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<!-- START PAGE CONTAINER -->
<div class="page-container">

    <!-- START PAGE SIDEBAR -->
    <div class="page-sidebar">
        <!-- START X-NAVIGATION -->
        <ul class="x-navigation">
            <li class="xn-logo">
                <a href="index.html">RR-CODE</a>
                <a href="#" class="x-navigation-control"></a>
            </li>
            <li class="xn-title">Menú</li>
            <li>
                <a href="index.php"><span class="fa fa-desktop"></span> <span class="xn-text">Inicio</span></a>
            </li>
            <li class="xn-openable">
                <a href="#"><span class="fa fa-files-o"></span> <span class="xn-text">Paginas</span></a>
                <ul>
                    <li><a href="index.php?r=persona"><span class="fa fa-eye"></span> ABM</a></li>
                    <li><a href="index.php?r=site/about"><span class="fa fa-wrench"></span> Sobre Nosotros</a></li>
                    <li><a href="index.php?r=site/contact"><span class="fa fa-user"></span> Contactos</a></li>
                </ul>
            </li>
        </ul>
        <!-- END X-NAVIGATION -->
    </div>
    <!-- END PAGE SIDEBAR -->

    <!-- PAGE CONTENT -->
    <div class="page-content">
      <!-- START X-NAVIGATION VERTICAL -->
      <ul class="x-navigation x-navigation-horizontal x-navigation-panel">
          <!-- TOGGLE NAVIGATION -->
          <li class="xn-icon-button">
              <a href="#" class="x-navigation-minimize"><span class="fa fa-dedent"></span></a>
          </li>
          <!-- END TOGGLE NAVIGATION -->
          <!-- SEARCH -->
          <li class="xn-search">
              <form role="form">
                  <input type="text" name="search" placeholder="Search..."/>
              </form>
          </li>
          <!-- END SEARCH -->
          <!-- POWER OFF -->
          <li class="xn-icon-button pull-right last">
              <a href="#"><span class="fa fa-power-off"></span></a>

              <ul class="xn-drop-left animated zoomIn">
                <?=Html::beginForm(['/site/logout'], 'post',['id'=>'form-logout']);?>
                  <li>
                    <a href="javascript:void(0);" onclick="document.getElementById('form-logout').submit();" class="mb-control"><span class="fa fa-sign-out"></span> Salir</a></li>
                    <?=Html::endForm();?>
              </ul>

          </li>
      </ul>
      <!-- END X-NAVIGATION VERTICAL -->
        <?= $content ?>
    </div>
<!-- END PAGE CONTENT -->
</div>
<!-- END PAGE CONTAINER -->
  <!-- MESSAGE BOX-->
  <div class="message-box animated fadeIn" data-sound="alert" id="mb-signout">
      <div class="mb-container">
          <div class="mb-middle">
              <div class="mb-title"><span class="fa fa-sign-out"></span> Log <strong>Out</strong> ?</div>
              <div class="mb-content">
                  <p>Are you sure you want to log out?</p>
                  <p>Press No if youwant to continue work. Press Yes to logout current user.</p>
              </div>
              <div class="mb-footer">
                  <div class="pull-right">
                      <a href="pages-login.html" class="btn btn-success btn-lg">Yes</a>
                      <button class="btn btn-default btn-lg mb-control-close">No</button>
                  </div>
              </div>
          </div>
      </div>
  </div>
  <!-- END MESSAGE BOX-->
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
