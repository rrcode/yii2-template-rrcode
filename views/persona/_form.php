<?php

use yii\helpers\Html;
use kartik\widgets\ActiveForm;
use yii\jui\DatePicker;
use kartik\builder\Form;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model app\models\Persona */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="persona-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombres')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellido_paterno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'apellido_materno')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ci')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'correo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'telefono')->textInput() ?>

    <?= $form->field($model, 'movil')->textInput() ?>

    <?= $form->field($model, 'genero')->dropDownList(['M' => 'Masculino', 'F' => 'Femenino', 'G' => 'Dam']); ?>

    <?= $form->field($model, 'estado_civil')->dropDownList(['C' => 'Casado(a)', 'S' => 'Soltero(a)', 'V' => 'Viudo(a)']); ?>

    <?= $form->field($model, 'fecha_nacimiento')->textInput() ?>

    <?php
    echo DatePicker::widget([
    'model' => $model,
    'attribute' => 'fecha_nacimiento',
    //'language' => 'ru',
    //'dateFormat' => 'yyyy-MM-dd',
]);
    ?>
    <?= Form::widget ( [ 
                    'model' => $model,
                    'form' => $form,
                    'columns' => 3,
                    'attributes' => [ 
                            'fecha_nacimiento' => [ 
                                    'type' => Form::INPUT_WIDGET,
                                    'widgetClass' => DateControl::classname (),
                                    'language' => 'es',
                                    'options' => [ 
                                            'language' => 'es',
                                            'type' => DateControl::FORMAT_DATETIME 
                                    ] 
                            ],
                    ] 
            ] );?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
