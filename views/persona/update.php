<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Persona */

$this->title = 'Update Persona: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Personas', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="index.php">Inicio</a></li>
    <li>Editar</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">
    <h2><span class="fa fa-arrow-circle-o-left"></span> <?= Html::encode($this->title) ?></h2>
</div>
<!-- END PAGE TITLE -->

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">

            <!-- START VERTICAL FORM SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-body">
                  <?= $this->render('_form', [
                      'model' => $model,
                  ]) ?>
                </div>
            </div>
            <!-- END VERTICAL FORM SAMPLE -->

        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
