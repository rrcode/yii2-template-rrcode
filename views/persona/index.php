<?php

use yii\helpers\Html;
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;

$this->title = 'Personas';
$this->params['breadcrumbs'][] = $this->title;
?>

<!-- START BREADCRUMB -->
<ul class="breadcrumb">
    <li><a href="index.php">Inicio</a></li>
    <li>Lista Personas</li>
</ul>
<!-- END BREADCRUMB -->

<!-- PAGE TITLE -->
<div class="page-title">
    <h2><span class="fa fa-arrow-circle-o-left"></span> <?=$this->title ?></h2>
</div>
<!-- END PAGE TITLE -->

<!-- PAGE CONTENT WRAPPER -->
<div class="page-content-wrap">

    <div class="row">
        <div class="col-md-12">

            <!-- START VERTICAL FORM SAMPLE -->
            <div class="panel panel-default">
                <div class="panel-body">
                  <h3><?= Html::encode($this->title) ?></h3>

                  <p>
                      <?= Html::a('Create Persona', ['create'], ['class' => 'btn btn-success']) ?>
                  </p>
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            'id',
                            'nombres',
                            'apellido_paterno',
                            'apellido_materno',
                            'ci',
                            // 'correo',
                            // 'telefono',
                            // 'movil',

                            ['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>
            </div>
            <!-- END VERTICAL FORM SAMPLE -->

        </div>
    </div>
</div>
<!-- END PAGE CONTENT WRAPPER -->
